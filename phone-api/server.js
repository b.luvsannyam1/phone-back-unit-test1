const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const connectDB = require("./config/db");
const errorHandler = require("./middlewares/errorHandler");
//load env
dotenv.config({ path: "./config/config.env" });

//connection with database
connectDB();

//app var
const app = express();

//Body parser
app.use(express.json());

//logger
if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
}

const PORT = process.env.PORT || 8000;

const server = app.listen(
    PORT,
    console.log(
        `server running in ${process.env.NODE_ENV}  mode on prop ${PORT}`
    )
);
//routes
const phoneapi = require("./routes/phone-api");

const account = require("./routes/accid");
const auth = require("./routes/auth");

//mount routers
app.use("/api/phoneapi", phoneapi);

app.use("/api/auth", auth);

app.use("/api/accounts", account);
app.use(errorHandler);

//handle rejections

process.on("unhandledRejection", (err, promise) => {
    console.log(`Error${err.message}`);
    //close server
    server.close(() => process.exit(1));
});
