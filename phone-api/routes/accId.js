const express = require("express");
const router = express.Router();
const {
    getAccounts,
    createAccount,
    getAccount,
    updateAccounts,
    deleteAccounts,
} = require("../controllers/accId");

router.route("/").get(getAccounts).post(createAccount);
router.route("/:id").get(getAccount).put(updateAccounts).delete(deleteAccounts);
module.exports = router;
