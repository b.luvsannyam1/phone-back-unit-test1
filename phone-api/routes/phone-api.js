const express = require("express");
const router = express.Router();
const {
    getPhoneapis,
    createPhoneapi,
    getPhoneapi,
    updatePhoneapis,
    deletePhoneapis,
} = require("../controllers/phone-api");

router.route("/").get(getPhoneapis).post(createPhoneapi);
router
    .route("/:id")
    .get(getPhoneapi)
    .put(updatePhoneapis)
    .delete(deletePhoneapis);
module.exports = router;
