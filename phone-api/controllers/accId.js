// @desc Get all phone
// @route Get all /users

const Accounts = require("../models/accId");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middlewares/async");
// @access public
exports.getAccounts = asyncHandler(async (req, res, next) => {
    const accounts = await Accounts.find();
    res.status(200).json({ success: true, data: accounts });
});
exports.updateAccounts = asyncHandler(async (req, res, next) => {
    const accounts = await Accounts.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });
    if (!accounts) {
        return res.status(400).json({ success: false });
    }
    res.status(200).json({ success: true, data: accounts });
});

exports.deleteAccounts = asyncHandler(async (req, res, next) => {
    const accounts = await Accounts.findByIdAndDelete(req.params.id);
    if (!accounts) {
        return res.status(400).json({ success: false });
    }
    res.status(200).json({ success: true, data: {} });
});

exports.getAccount = asyncHandler(async (req, res, next) => {
    const accounts = await Accounts.findById(req.params.id);
    if (!accounts) {
        next(
            new ErrorResponse(
                `Accounts not found with id of ${req.params.id}`,
                404
            )
        );
    }
    res.status(200).json({ success: true, data: accounts });
});

// @desc create all phone
// @route create all /users
// @access private
exports.createAccount = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    const accounts = await Accounts.create(req.body);
    res.status(201).json({ success: true, data: accounts });
});
