const Phoneapi = require("../models/phoneapi");
const ErrorResponse = require("../utils/errorResponse");
const User = require("../models/users");
const asyncHandler = require("../middlewares/async");

//@desc Register user
//@route GET /api/auth/register
exports.register = asyncHandler(async (req, res, next) => {
    const { name, email, password, role } = req.body;

    const user = await User.create({
        name,
        email,
        password,
        role,
    });
    res.status(200).json({ success: true });
});
