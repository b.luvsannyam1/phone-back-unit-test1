// @desc Get all phone
// @route Get all /users

const Phoneapi = require("../models/phoneapi");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middlewares/async");
// @access public
exports.getPhoneapis = asyncHandler(async (req, res, next) => {
    const phoneapi = await Phoneapi.find();
    res.status(200).json({ success: true, data: phoneapi });
});
exports.updatePhoneapis = asyncHandler(async (req, res, next) => {
    const phoneapi = await Phoneapi.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });
    if (!phoneapi) {
        return res.status(400).json({ success: false });
    }
    res.status(200).json({ success: true, data: phoneapi });
});

exports.deletePhoneapis = asyncHandler(async (req, res, next) => {
    const phoneapi = await Phoneapi.findByIdAndDelete(req.params.id);
    if (!phoneapi) {
        return res.status(400).json({ success: false });
    }
    res.status(200).json({ success: true, data: {} });
});

exports.getPhoneapi = asyncHandler(async (req, res, next) => {
    const phoneapi = await Phoneapi.findById(req.params.id);
    if (!phoneapi) {
        next(
            new ErrorResponse(
                `Phoneapi not found with id of ${req.params.id}`,
                404
            )
        );
    }
    res.status(200).json({ success: true, data: phoneapi });
});

// @desc create all phone
// @route create all /users
// @access private
exports.createPhoneapi = asyncHandler(async (req, res, next) => {
    console.log(req.body);
    const phoneapi = await Phoneapi.create(req.body);
    res.status(201).json({ success: true, data: phoneapi });
});
