const mongoose = require("mongoose");

const PhoneapiSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please add a name"],
        maxlength: [50, "Name can not be more than 50 chars"],
    },
    slug: String,
    description: {
        type: String,
        required: [true, "Please add a description"],
        maxlength: [500, "Description can not be more than 500 chars"],
    },
});
module.exports = mongoose.model("PhoneApi", PhoneapiSchema);
