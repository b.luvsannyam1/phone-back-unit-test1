const mongoose = require("mongoose");

const Accounts = new mongoose.Schema({
    AccId: {
        type: String,
        required: [true, "Please add a name"]
    },
    ClientId: {
        type: String,
        required: [true, "ClientId not generated"],
        maxlength: [500, "Description can not be more than 500 chars"],
    },
    Balance:{
        type: Number,
        required: [true,"Balance is empty"]
    },
    ClientId: {
        type: String,
        required: [true, "CardUid empty"]
    },
    
});
module.exports = mongoose.model("Accounts", Accounts);
