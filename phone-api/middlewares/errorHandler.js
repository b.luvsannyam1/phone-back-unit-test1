const ErrorResponse = require("../utils/errorResponse");

const errorHandler = (err, req, res, next) => {
    let error = { ...err };

    error.message = err.message;
    if (err.name === "CastError") {
        const message = `Phoneapi not found with id of ${err.value}`;
        error = new ErrorResponse(message, 404);
    }
    res.status(err.statusCode || 500).json({
        success: false,
        error: err.message || `Default error`,
    });
};

module.exports = errorHandler;
